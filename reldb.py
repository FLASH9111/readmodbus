import json
import os
import time

from contextlib import contextmanager

from psycopg2.extras import RealDictCursor
from psycopg2.pool import ThreadedConnectionPool


class RelDB:
    def __init__(self):
        ROOT_DIR = os.path.dirname(os.path.abspath(__file__))

        with open(ROOT_DIR+'/config.json') as f:
            data_config = json.load(f);

        config_postgres = data_config.get('rel_db')

        try:
            self.__pool = ThreadedConnectionPool(1, 10,
                                                 database=config_postgres.get("database"),
                                                 user=config_postgres.get("user"),
                                                 password=config_postgres.get("password"),
                                                 host=config_postgres.get("host"),
                                                 port=config_postgres.get("port"))
        except Exception as e:
            print(e)
            print("Reconnect")
            time.sleep(5)
            try:
                self.__pool = ThreadedConnectionPool(1, 10,
                                                     database=config_postgres.get("database"),
                                                     user=config_postgres.get("user"),
                                                     password=config_postgres.get("password"),
                                                     host=config_postgres.get("host"),
                                                     port=config_postgres.get("port"))
            except:
                return False
            # exit()
        finally:
            # closing database connection.
            # use closeall method to close all the active connection if you want to turn of the application
            if hasattr(self, '__pool'):
                if (self.__pool):
                    self.__pool.closeall
    @contextmanager
    def get_db_connection(self):
        try:
            connection = self.__pool.getconn()
            yield connection
        except:
            return False
        finally:
            self.__pool.putconn(connection)

    @contextmanager
    def get_db_cursor(self, commit=False):
        with self.get_db_connection() as connection:
            cursor = connection.cursor(cursor_factory=RealDictCursor)
            try:
                yield cursor
                if commit:
                    connection.commit()
            except:
                return False
            finally:
                cursor.close()