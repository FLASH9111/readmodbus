# -*- coding: utf-8 -*-
import json
import math
import os
import threading
import time
import signal
import schedule

from pymodbus.client.sync import ModbusTcpClient

from mathoperation import MathOperation
from reldb import RelDB
from writeinflux import WriteInflux
from sys import platform


class ReadTCP:

    def __init__(self):
        ROOT_DIR = os.path.dirname(os.path.abspath(__file__))

        with open(ROOT_DIR+'/config.json') as f:
            data_config = json.load(f)

        config_modbustcp = data_config.get('modbusTCP')
        self.config_modbustcp = config_modbustcp

        self.number_thread = int(config_modbustcp['number_thread'])
        self.max_timeout = int(config_modbustcp['max_timeout'])
        self.rel_db = RelDB()
        self.polling_times = self.all_polling_times()

    def get_polling_modbustcp_devices(self, polling_time_value):
        """
        Получает все устройства с заданным таймаутом опроса
        :param polling_time_value:
        :return:
        """
        rel_db = self.rel_db
        if not rel_db: return []
        with rel_db.get_db_cursor() as cursor:
            cursor.execute(
                f"SELECT devices.id_devices, devices.status, devices.unit, devices.polling_time, modbustcp_parameters.* FROM devices LEFT JOIN protocol_types on protocol_types.id_protocol_types = devices.id_protocol_types_protocol_types LEFT JOIN modbustcp_parameters on modbustcp_parameters.id_devices_devices = devices.id_devices WHERE protocol_types.type = 'modbustcp' AND devices.polling_time = {polling_time_value} AND devices.status ORDER BY devices.polling_time")
            return cursor.fetchall()
        return []

    #
    def polling_devices(self, polling_time_value):
        """
        Опрашивает регистры устройства ModbusTCP
        :param polling_time_value:
        :return:
        """
        try:
            devices = self.get_polling_modbustcp_devices(polling_time_value)
            math_operation = MathOperation()
        except:
            return
        if devices != []:
            for device in devices:
                try:
                    if platform == "linux" or platform == "linux2" or platform == "darwin":
                        signal.alarm(self.max_timeout)
                    try:
                        registers = self.get_modbustcp_registers(device['id_devices'])
                    except:
                        continue
                    if registers != [] and device['ip_address'] != '' and device['status']:
                        client = ModbusTcpClient(device['ip_address'])
                        # client = ModbusClient(method="rtu", port="/dev/ttyS0", stopbits=1, bytesize=8, parity='E', baudrate = 9600)
                        if not (client.connect()):
                            print("Connection not established")
                            continue

                        now_time = int(time.time() * 1000000000)
                        if device['unit']:
                            unit = int(device['unit'])
                        else:
                            unit = 0
                        for register in registers:
                            # print(register)
                            physical_address = int(register['physical_address'], 16)
                            if register['function_code'] == '4':
                                rr = client.read_input_registers(physical_address, 1, unit=unit)
                                reads = rr.registers
                            else:
                                if register['function_code'] == '3':
                                    rr = client.read_holding_registers(physical_address, 1, unit=unit)
                                    reads = rr.registers
                                else:
                                    if register['function_code'] == '2':
                                        rr = client.read_discrete_inputs(physical_address, 1, unit=unit)
                                        reads = rr.bits
                                    else:
                                        print('This function is not supported')
                                        continue
                            try:
                                if reads[0]:
                                    print(reads[0])
                                    measurement = f"device_{device['id_devices']}"
                                    field_name = f"meas_{register['physical_address']}"
                                    if register['formula'] and register['formula'] != '':
                                        field_value = round(float(math_operation.formula(register['formula'], reads[0])), 2)
                                    else:
                                        field_value = round(float(reads[0]), 2)
                                    # field_value = round(float(reads[0] * 0.01 - 327), 2)
                                    time_influx = now_time
                                    # print(time_influx)
                                    json_body = [
                                        {
                                            "measurement": measurement,
                                            "time": time_influx,
                                            "fields": {
                                                field_name: field_value
                                            }
                                        }]
                                    insert = WriteInflux(json_body)
                            except Exception as e:
                                print(e)
                                return

                        client.close()
                    if platform == "linux" or platform == "linux2" or platform == "darwin":
                        signal.alarm(0)
                except Exception as e:
                    print(e)
                    print('Modbus connection is too slow!')

    def schedule_function(self, polling_time_value):
        self.polling_devices(polling_time_value)
        schedule.every(polling_time_value).seconds.do(self.polling_devices, polling_time_value)

    def schedule_compare(self):
        schedule.every(5).minutes.do(self.compare_polling_times)

    def all_polling_times(self):
        """
        Получает все таймауты устройств
        :return:polling_times
        """
        try:
            rel_db = self.rel_db
        except:
            return[]
        if not rel_db: return []
        with rel_db.get_db_cursor() as cursor:
            query = f"SELECT count(*), devices.polling_time FROM devices LEFT JOIN protocol_types on protocol_types.id_protocol_types = devices.id_protocol_types_protocol_types WHERE protocol_types.type = 'modbustcp' AND devices.status GROUP BY devices.polling_time"
            cursor.execute(query)
            polling_times = cursor.fetchall()
            return polling_times
        return []

    def get_modbustcp_registers(self, id_devices):
        """
        Получает все регистры устройства
        :param id_devices:
        :return: registers
        """
        rel_db = self.rel_db  # RelDB()
        if not rel_db: return []
        with rel_db.get_db_cursor() as cursor:
            query = f"SELECT modbus_registers.physical_address, modbus_registers.formula, modbus_registers.channel_name, modbus_registers.id_modbus_registers, modbus_functions.function_code FROM modbus_registers LEFT JOIN devices on modbus_registers.id_devices_devices = devices.id_devices LEFT JOIN modbus_functions on modbus_functions.id_modbus_functions = modbus_registers.id_modbus_functions_modbus_functions WHERE devices.id_devices = {id_devices}"
            cursor.execute(query)
            registers = cursor.fetchall()
            return registers
        return []

    def compare_polling_times(self):
        """
        Сравнивает текущий и новый набор таймаутов
        :return:
        """
        try:
            new_polling_times = self.all_polling_times()
        except:
            return
        old_times = list()
        for time in self.polling_times:
            old_times.append(time['polling_time'])

        new_times = list()

        for time in new_polling_times:
            new_times.append(time['polling_time'])

        compare = list(set(new_times) - set(old_times))

        if len(compare) > 0:
            compare_polling_times = []
            new_threads = list()

            for time in compare:
                for new_polling_time in new_polling_times:
                    if time == new_polling_time['polling_time']:
                        compare_polling_times.append(new_polling_time)

            for polling_time in compare_polling_times:
                polling_time_value = int(polling_time['polling_time'])
                if self.number_thread != 0:
                    count_thread = math.ceil(polling_time['count'] / self.number_thread)
                else:
                    count_thread = 1
                for i in range(count_thread):
                    x = threading.Thread(target=self.schedule_function, args=(polling_time_value,))
                    new_threads.append(x)
                    x.start()

            # for x in new_threads:
            #     x.join()

            self.polling_times = new_polling_times

    def main(self):
        threads = list()
        compare_thread = threading.Thread(target=self.schedule_compare)
        compare_thread.start()
        for polling_time in self.polling_times:
            polling_time_value = int(polling_time['polling_time'])
            if self.number_thread != 0:
                count_thread = math.ceil(polling_time['count'] / self.number_thread)
            else:
                count_thread = 1
            for i in range(count_thread):
                x = threading.Thread(target=self.schedule_function, args=(polling_time_value,))
                threads.append(x)
                x.start()

        for x in threads:
            x.join()

        while True:
            schedule.run_pending()
            time.sleep(1)
