import numexpr as ne


class MathOperation:
    def formula(self, expression, x):
        """
        Вычисление формулы для x
        """
        try:
            return ne.evaluate(expression)
        except Exception as err:
            return 0
